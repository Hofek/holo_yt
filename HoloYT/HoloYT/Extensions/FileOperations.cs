﻿using HoloYT.Model;
using HoloYT.ViewModel;
using System;
using System.IO;
using System.Xml.Serialization;

namespace HoloYT.Extensions
{
    public static class FileOperations
    {
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        public static bool MoveToDestination(this MediaViewModel mediaVm, string path = null)
        {
            try
            {
                if (String.IsNullOrEmpty(path))
                {
                    string src = Environment.ExpandEnvironmentVariables(Properties.Settings.Default.TempFolder) + "\\" + mediaVm.Name + "." + mediaVm.Media.Extension; ;
                    string dest = String.Empty;
                    if (mediaVm.AudioOnly == false)
                    {
                        dest = Path.Combine(Config.Instance.VideoPath, (mediaVm.Name + "." + mediaVm.Media.Extension));
                    }
                    else
                    {
                        dest = Path.Combine(Config.Instance.AudioPath, mediaVm.Name + "." + mediaVm.Media.Extension);
                    }

                    if (File.Exists(dest)) File.Delete(dest);
                    File.Move(src, dest);
                }

                else
                {
                    string src = Environment.ExpandEnvironmentVariables(Properties.Settings.Default.TempFolder) + "\\" + mediaVm.Name + "." + mediaVm.Media.Extension;
                    string dest = path + "\\" + mediaVm.Name + "." + mediaVm.Media.Extension;

                    if (File.Exists(dest)) File.Delete(dest);
                    File.Move(src, dest);
                }

                return true;
            }

            catch (Exception e)
            {
                _logger.Error(e, "[Błąd] Nie udało się przenieść plików z katalogu tymczasowego.");                
                return false;
            }
        }        
    }
}
