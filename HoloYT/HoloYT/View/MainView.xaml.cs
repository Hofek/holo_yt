﻿using HoloYT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HoloYT.View
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : UserControl
    {
        public MainView()
        {
            InitializeComponent();
        }
      
        private void DataGridRow_MouseRightButtonDown(object sender, MouseEventArgs e)
        {
            int index = Main_DataGrid.ItemContainerGenerator.IndexFromContainer((DataGridRow)sender);
            Main_DataGrid.SelectedIndex = index;
        }
   
        private void Main_DataGrid_Unloaded(object sender, RoutedEventArgs e)
        {
            DataGrid dg = sender as DataGrid;
            dg.CommitEdit();
        }  

        private void ComboBox_IsHitTestVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;           
            cb.Items.Refresh();         
        }
    }
}
