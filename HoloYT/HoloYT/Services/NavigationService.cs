﻿using HoloYT.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace HoloYT.Services
{
    public class NavigationService : INavigationService
    {
        private readonly List<Window> _windows = new List<Window>();
        public void CloseWindow(object content)
        {
            var window = _windows.FirstOrDefault(w => w.Content == content);
            if (window == null)
                return;

            window.Close();
            _windows.Remove(window);
        }

        public void OpenWindow(object content)
        {
            var window = new Window()
            {
                Owner = App.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner, 
                WindowStyle = WindowStyle.None, 
                AllowsTransparency = true, 
                ResizeMode=ResizeMode.CanResizeWithGrip, 
                Background= new ImageBrush(new BitmapImage(new Uri("pack://application:,,,/Resources/MainBackground.jpg"))),
                Content = content,
                SizeToContent = SizeToContent.WidthAndHeight
            };
            window.Closed += Window_Closed;
            _windows.Add(window);
            window.Show();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (sender is Window)
                CloseWindow(((Window)sender).Content);
        }
    }
}
