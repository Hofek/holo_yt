﻿using HoloYT.Model.Interfaces;

namespace HoloYT.Services
{
    class DummyHelpDataService : IHelpDataService
    {
        public string GetDllVersion()
        {
            return "2019.05.17";
        }

        public string GetProgramVersion()
        {
            return "1.0.0";
        }
    }
}
