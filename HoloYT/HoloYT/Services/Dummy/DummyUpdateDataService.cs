﻿using HoloYT.Model.Interfaces;

namespace HoloYT.Services
{
    class DummyUpdateDataService : IUpdateDataService
    {    
        public string GetChangelog()
        {
            return @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut massa metus, molestie id sodales porta, consequat a odio. Praesent leo sapien, pharetra vitae tristique nec, commodo sed velit. Praesent vel augue aliquam, tempor nunc et, rutrum est. Nunc vel lectus tincidunt, porttitor lorem in, faucibus nulla. Etiam vulputate ligula vel tortor aliquam, eleifend dictum nulla condimentum. Phasellus cursus ex orci, non iaculis justo tincidunt id. Curabitur venenatis massa nec mattis congue. Nulla sagittis augue ac efficitur lacinia. Aliquam erat volutpat. In porttitor vel mi in maximus. Nunc urna arcu, commodo eget orci quis, bibendum interdum mauris. Suspendisse lorem odio, tincidunt a tellus sit amet, pretium tempor augue. Aliquam lacus augue, semper a augue sed, dictum vestibulum metus. Ut et egestas felis. Maecenas vel augue pretium, tempor orci faucibus, dictum nibh.";
        }

        public string GetChangelogTitle()
        {
            return "1.0.4";
        }
    }
}
