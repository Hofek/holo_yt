﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using HoloYT.Model;
using HoloYT.Model.Args;
using HoloYT.Model.Interfaces;
using HoloYT.ViewModel;

namespace HoloYT.Services
{
    class DummyDataService : IDataService
    {
        bool IDataService.IsInAdvancedMode => true;

        bool IDataService.IsUIBlocked => true;

        bool IDataService.IsConsoleEnabled => true;

        public event EventHandler<BlockUIArgs> BlockUIChanged;

        public bool IsInAdvancedMode()
        {
            return false;
        }

        public bool IsUIBlocked()
        {
            return false;
        }        

        public bool IsConsoleEnabled()
        {
            return true;
        }

        public IList<MediaViewModel> GetItems()
        {
            return new ObservableCollection<ViewModel.MediaViewModel>()
            {
                new MediaViewModel(null, new Model.Media() { Name="R3HAB x Mike Williams - Lullaby"}) { State= Enums.MediaState.complete, Url = "https://www.youtube.com/watch?v=cTVmdwZA738" },
                new MediaViewModel(null, new Model.Media() { Name="K-391 & Alan Walker - Ignite"}) { State= Enums.MediaState.error, Url = "https://www.youtube.com/watch?v=cTVmdwZA738" },
                new MediaViewModel(null, new Model.Media() { Name="Alan Walker - Diamond Heart"}) { State= Enums.MediaState.complete, Url = "https://www.youtube.com/watch?v=cTVmdwZA738" },
                new MediaViewModel(null, new Model.Media() { Name="Alan Walker - Diamond Heart"}) { State= Enums.MediaState.download, Url = "https://www.youtube.com/watch?v=cTVmdwZA738", Progress=10 }
            };
        }

        public Task<bool> UpdateLibrary()
        {
            throw new NotImplementedException();
        }

        public Task<bool> GetMeta(MediaViewModel media)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Download(MediaViewModel media, string path = null)
        {
            throw new NotImplementedException();
        }

        public Task<bool> VerifyOperation(IList<MediaViewModel> medias)
        {
            throw new NotImplementedException();
        }
    }
}
