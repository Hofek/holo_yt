﻿using HoloYT.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace HoloYT.Services
{
    public class ConverterService
    {
        private string arguments = String.Empty;
        private readonly string SdImx50Cmd = "-i \"@input\" -f lavfi -i anullsrc=channel_layout=stereo:sample_rate=48000 -vsync cfr -r 25 -pix_fmt yuv422p -c:v mpeg2video -c:a @audio -vf [in]scale=0:576:force_original_aspect_ratio=decrease,setsar=1,pad=1024:576:(ow-ih)/2:0[middle];[middle]scale=720:576:-1,pad=720:608:(ow-ih)/2:32[out] -aspect 16:9 -minrate 50000k -maxrate 50000k -b:v 50000k -intra -top 1 -flags +ildct+low_delay -dc 10 -ps 1 -qmin 1 -qmax 3 -bufsize 2000000 -rc_init_occupancy 2000000 -intra_vlc 1 -non_linear_quant 1 -color_primaries 5 -color_trc 1 -colorspace 5 -rc_max_vbv_use 1 -tag:v mx5p -d10_channelcount @channelCount -shortest -map 0:0 -map 0:1 \"@output\"";
        private readonly string UniversalCmd = "-i \"@input\" -c:v @video -c:a @audio @output";

        private string BuildArguments(Media media, bool AdvancedMode = false)
        {
            string cmd = null;
            if (media.TargetFormat?.VideoCodec == Enum.GetName(typeof(Enums.VideoCodec), Enums.VideoCodec.SDQuicktimeIMX50))
            {
                var videoCodec = Enum.Parse(typeof(Enums.VideoCodec), AdvancedMode ? media.TargetFormat.VideoCodec : Config.Instance.TargetFormat.VideoCodec);
                var audioCodec = Enum.Parse(typeof(Enums.AudioCodec), AdvancedMode ? media.TargetFormat.AudioCodec : Config.Instance.TargetFormat.AudioCodec);
                switch (videoCodec)
                {
                    case Enums.VideoCodec.SDQuicktimeIMX50:
                        cmd =  SdImx50Cmd.Replace("@input", media.Name + media.Extension)
                                         .Replace("@channelCount", AdvancedMode ? media.TargetFormat.ChannelCount.ToString() : Config.Instance.TargetFormat.ChannelCount.ToString());
                        break;
                    case Enums.VideoCodec.libx264:
                        cmd = UniversalCmd.Replace("@input", media.Name + media.Extension).Replace("@video", "libx264");
                        break;
                    case Enums.VideoCodec.copy:
                        cmd = UniversalCmd.Replace("@input", media.Name + media.Extension).Replace("@video", "copy");
                        break;
                    case Enums.VideoCodec.ProRes:
                        cmd = UniversalCmd.Replace("@input", media.Name + media.Extension).Replace("@video", "prores_ks");
                        break;

                }

                switch(audioCodec)
                {
                    case Enums.AudioCodec.aac:
                        cmd = cmd.Replace("@audio", "aac");
                        break;
                    case Enums.AudioCodec.copy:
                        cmd = cmd.Replace("@audio", "copy");
                        break;
                    case Enums.AudioCodec.pcm_s16le:
                        cmd = cmd.Replace("@audio", "pcm_s16le");
                        break;
                    case Enums.AudioCodec.pcm_s32le:
                        cmd = cmd.Replace("@audio", "pcm_s32le");
                        break;
                }

                cmd = cmd.Replace("@output", String.Concat(media.Name, '.', AdvancedMode ? media.TargetFormat.Container : Config.Instance.TargetFormat.Container));
            }
            return cmd;            
        }
        public virtual Task ConvertToTarget(Media l)
        {            
            var taskState = new TaskCompletionSource<bool>();
            if (!l.Convert)
            {
                taskState.SetResult(true);
                return taskState.Task;
            }

            Process proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "api/ffmpeg.exe",
                    Arguments = BuildArguments(l),
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true
                }
            };
            proc.EnableRaisingEvents = true;
            proc.OutputDataReceived += (sender, e) =>
            {

                if (String.IsNullOrEmpty(e.Data))
                    return;


            };
            proc.Exited += (s, e) => { taskState.SetResult(true); };
            proc.ErrorDataReceived += new DataReceivedEventHandler((sender, e) =>
            {
                if (String.IsNullOrEmpty(e.Data))
                    return;

            });

            proc.Start();
            proc.BeginErrorReadLine();
            proc.BeginOutputReadLine();

            return taskState.Task;
        }
    }
}
