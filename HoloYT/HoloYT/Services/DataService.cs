﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using HoloYT.Extensions;
using HoloYT.MediaProviders;
using HoloYT.Model;
using HoloYT.Model.Interfaces;
using HoloYT.ViewModel;

namespace HoloYT.Services
{
    class DataService : IDataService
    {
        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();        

        public bool IsInAdvancedMode { get; private set; }
     
        public bool IsUIBlocked { get; private set; }              

        public bool IsConsoleEnabled { get; private set; }                
                
        public Task<bool> UpdateLibrary()
        {
            string line = String.Empty;           
            var taskStatus = new TaskCompletionSource<bool>();

            Process proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "api/youtube-dl.exe",
                    Arguments = "-U" + (String.IsNullOrEmpty(Config.Instance.Proxy) ? "" : (" --proxy " + Config.Instance.Proxy)),
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true
                }
            };
                        
            proc.OutputDataReceived += new DataReceivedEventHandler((sender, e) =>
            {
                if (!String.IsNullOrEmpty(e.Data))
                {
                    line = e.Data;
                    if (line.Contains("Updating"))
                    {
                        line = line.Remove(0, 20);
                        line = line.Substring(0, line.Length - 4);                       
                    }

                    else if (line.Contains("Updated"))
                        taskStatus.SetResult(true);

                    else if (line.Contains("is up-to-date"))
                        taskStatus.SetResult(false);
                }
            });
            proc.ErrorDataReceived += new DataReceivedEventHandler((sender, e) =>
            {
                if (!String.IsNullOrEmpty(e.Data))                
                    if (e.Data.ToLower().Contains("error"))                    
                        taskStatus.SetResult(false);                                    
            });

            _logger.Info($"Updating library started. Current version: {Config.Instance.LibraryVersion}");
            proc.Start();
            proc.BeginErrorReadLine();
            proc.BeginOutputReadLine();         
            _logger.Info("Updating library finished");
            return taskStatus.Task;
        }
        
        public async Task<bool> Download(MediaViewModel mediaVm, string path=null)
        {
            bool result = false;
            IMediaProvider provider = null;
            mediaVm.Media.Type = ParseMediaType(mediaVm.Url);
           
            switch (mediaVm.Media.Type)
            {
                case Enums.MediaType.youtube:
                    provider = new YoutubeProvider();
                    break;


                case Enums.MediaType.facebook:
                    provider = new FacebookProvider();
                    break;

            }

            if (provider == null)
            {
                mediaVm.State = Enums.MediaState.error;
                return false;
            }
                

            mediaVm.State = Enums.MediaState.download;
            result = await provider.DownloadMedia(mediaVm, !IsInAdvancedMode).ConfigureAwait(false);

            if (result && mediaVm.State != Enums.MediaState.error)
            {
                string localDownloadPath = path == null ? (mediaVm.AudioOnly ? Config.Instance.AudioPath : Config.Instance.VideoPath) : path;                

                mediaVm.State = Enums.MediaState.complete;
                if (String.IsNullOrEmpty(localDownloadPath))
                {
                    if (!mediaVm.MoveToDestination())
                    {
                        mediaVm.State = Enums.MediaState.error;
                        return false;
                    }
                }
                else
                    if (!mediaVm.MoveToDestination(localDownloadPath))
                    {
                        mediaVm.State = Enums.MediaState.error;
                        return false;
                    }
            }

            return true;
        }
        
        public async Task<bool> GetMeta(MediaViewModel mediaVm)
        {
            bool result = false;
            IMediaProvider provider = null;

            mediaVm.Media.Type = ParseMediaType(mediaVm.Url);
             
            switch (mediaVm.Media.Type)
            {
                case Enums.MediaType.youtube:
                    provider = new YoutubeProvider();
                    break;


                case Enums.MediaType.facebook:
                    provider = new FacebookProvider();
                    break;
            }

            if (provider == null)
            {
                mediaVm.State = Enums.MediaState.error;
                return false;
            }

            mediaVm.State = Enums.MediaState.getInfo;
            result = await provider.GetMediaInfo(mediaVm).ConfigureAwait(false);

            if (result && mediaVm.State != Enums.MediaState.error)
            {
                if (mediaVm.Video.Count > 0) mediaVm.SelectedVideoFormat = mediaVm.Video[mediaVm.Video.Count - 1];
                if (mediaVm.Audio.Count > 0) mediaVm.SelectedAudioFormat = mediaVm.Audio[mediaVm.Audio.Count - 1];

                _ = Application.Current.Dispatcher.BeginInvoke((Action)(() => { mediaVm.State = Enums.MediaState.none; }));
            }
            else
                return false;

            return true;
        }

        private Enums.MediaType ParseMediaType(string url)
        {
            if (url.Contains("youtube.") || url.Contains("youtu."))
                return Enums.MediaType.youtube;
            else if (url.Contains("facebook.") || (url.Contains("fb.")))
                return Enums.MediaType.facebook;

            return Enums.MediaType.unknown;
        }

        public async Task<bool> VerifyOperation(IList<MediaViewModel> medias)
        {
            if (medias.Any(l => l.State == Enums.MediaState.error))
            {
                foreach (var l in medias.Where(l => l.State == Enums.MediaState.error))                
                    l.State = Enums.MediaState.update;
                                
                if (await UpdateLibrary().ConfigureAwait(false))                                                     
                    return false;
                
                else
                {
                    foreach (MediaViewModel l in medias.Where(l => l.State == Enums.MediaState.update))                    
                        l.State = Enums.MediaState.error;                                        
                }
            }
            return true;
        }


    }
}
