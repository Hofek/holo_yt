﻿using HoloYT.Model.Interfaces;
using System;
using System.Net;

namespace HoloYT.Services
{
    public class UpdateDataService : IUpdateDataService
    {
        public string GetChangelog()
        {
            string changelog = String.Empty;
            string url = "http://holo.szn.tvp.pl/updateYT/changelog.txt";

            using (WebClient wc = new WebClient())
            {
                changelog = wc.DownloadString(url);
            }
            return changelog;
        }

        public string GetChangelogTitle()
        {
            return "1.0.6";
        }
    }
}
