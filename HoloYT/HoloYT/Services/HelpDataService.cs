﻿using HoloYT.Model;
using HoloYT.Model.Interfaces;
using System;
using System.Diagnostics;
using System.Reflection;

namespace HoloYT.Services
{
    public class HelpDataService : IHelpDataService
    {        
        public string GetDllVersion()
        {
            string version = String.Empty;
            Process proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "api/youtube-dl.exe",
                    Arguments = "--version",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,                   
                    CreateNoWindow = true
                }
            };

            proc.OutputDataReceived += new DataReceivedEventHandler((sender, e) =>
            {
                if (!String.IsNullOrEmpty(e.Data))
                {
                    version = e.Data;
                }
            });           

            proc.Start();            
            proc.BeginOutputReadLine();
            proc.WaitForExit();
            Config.Instance.LibraryVersion = version;
            return version;
        }

        public string GetProgramVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }
    }
}
