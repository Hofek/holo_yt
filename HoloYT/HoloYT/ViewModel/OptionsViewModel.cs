﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using HoloYT.Messages;
using HoloYT.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;

namespace HoloYT.ViewModel
{
    public class OptionsViewModel : ViewModelBase
    {
        private string _videoPath = Config.Instance.VideoPath;
        private string _proxy = Config.Instance.Proxy;
        private string _audioPath = Config.Instance.AudioPath;
        private int _width = Int32.Parse(Config.Instance.TargetFormat.Resolution.Split('x')[0]);
        private int _height = Int32.Parse(Config.Instance.TargetFormat.Resolution.Split('x')[1]);
        private List<string> _containers = Enum.GetNames(typeof(Enums.Container)).ToList();        
        private string _selectedContainer;
        private List<string> _videoCodecs = Enum.GetNames(typeof(Enums.VideoCodec)).ToList();
        private string _selectedVideoCodec;
        private List<string> _audioCodecs = Enum.GetNames(typeof(Enums.AudioCodec)).ToList();
        private string _selectedAudioCodec;
        private List<string> _sampleRates = Enum.GetNames(typeof(Enums.SampleRate)).ToList();
        private string _selectedSampleRate;
        private int _channelCount = Config.Instance.TargetFormat.ChannelCount;

        public RelayCommand SwitchViewCommand_Main { get; set; }
        public RelayCommand SaveConfigCommand { get; set; }

        public string VideoPath
        {
            get => _videoPath;
            set => Set(ref _videoPath, value);
        }
        public string Proxy
        {
            get => _proxy;
            set => Set(ref _proxy, value);
        }
        public string AudioPath
        {
            get => _audioPath;
            set => Set(ref _audioPath, value);
        }
        public ICollectionView Containers { get; set; }
        public string SelectedContainer { get => _selectedContainer; set => Set(ref _selectedContainer, value); }
        public ICollectionView VideoCodecs { get; set; }
        public string SelectedVideoCodec { get => _selectedVideoCodec; set => Set(ref _selectedVideoCodec, value); }
        public ICollectionView AudioCodecs { get; set; }        
        public string SelectedAudioCodec { get => _selectedAudioCodec; set => Set(ref _selectedAudioCodec, value); }
        public int ChannelCount { get => _channelCount; set => Set(ref _channelCount, value); }
        public ICollectionView SampleRates { get; set; }
        public string SelectedSampleRate { get => _selectedSampleRate; set => Set(ref _selectedSampleRate, value); }
        public int Width { get => _width; set => Set(ref _width, value); }
        public int Height { get => _height; set => Set(ref _height, value); }

        public OptionsViewModel()
        {
            SwitchViewCommand_Main = new RelayCommand(SwitchViewToMain);
            SaveConfigCommand = new RelayCommand(SaveConfig, CanSaveConfig);            

            Containers = CollectionViewSource.GetDefaultView(_containers);
            AudioCodecs = CollectionViewSource.GetDefaultView(_audioCodecs);
            VideoCodecs = CollectionViewSource.GetDefaultView(_videoCodecs);
            SampleRates = CollectionViewSource.GetDefaultView(_sampleRates);
            SelectedAudioCodec = _audioCodecs.FirstOrDefault(codec => codec == Config.Instance.TargetFormat.AudioCodec) ?? _audioCodecs.First();
            SelectedVideoCodec = _videoCodecs.FirstOrDefault(codec => codec == Config.Instance.TargetFormat.VideoCodec) ?? _videoCodecs.First();
            SelectedSampleRate = _sampleRates.FirstOrDefault(codec => codec == Config.Instance.TargetFormat.SampleRate) ?? _sampleRates.First();
            SelectedContainer = _containers.FirstOrDefault(codec => codec == Config.Instance.TargetFormat.Container) ?? _containers.First();
        }

        private bool CanSaveConfig()
        {
            return true;
        }

        private void SaveConfig()
        {
            Config.Instance.VideoPath = _videoPath;
            Config.Instance.AudioPath = _audioPath;
            Config.Instance.Proxy = _proxy;
            Config.Instance.TargetFormat.VideoCodec = _selectedVideoCodec;
            Config.Instance.TargetFormat.AudioCodec = _selectedAudioCodec;
            Config.Instance.TargetFormat.Container = _selectedContainer;
            Config.Instance.TargetFormat.ChannelCount = _channelCount;
            Config.Instance.TargetFormat.SampleRate = _selectedSampleRate;
            Config.Instance.TargetFormat.Resolution = String.Concat(_width, "x", _height);

            Config.Save();
            SwitchViewToMain();
        }

        private void SwitchViewToMain()
        {
            Messenger.Default.Send<SwitchViewMessage>(new SwitchViewMessage() { View = "Main" });
        }
    }
}
