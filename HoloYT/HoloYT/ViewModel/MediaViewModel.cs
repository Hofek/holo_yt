﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using HoloYT.Model;
using HoloYT.Model.Formats;
using HoloYT.Model.Interfaces;
using System;
using System.Collections.Generic;
using static HoloYT.Model.Enums;

namespace HoloYT.ViewModel
{
    public class MediaViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        public readonly Model.Media Media;

        private string _url;
        public string Url
        {
            get => _url;
            set => Set(ref _url, value);
        }

        private string _name;
        public string Name { get => _name; set => Set(ref _name, value); }

        private MediaState _state;
        public MediaState State
        {
            get => _state;
            set => Set(ref _state, value);
        }

        private bool _audioOnly;
        public bool AudioOnly { get => _audioOnly; set => Set(ref _audioOnly, value); }

        private int _progress;
        public int Progress
        {
            get => _progress;
            set => Set(ref _progress, value);
        }

        private List<VideoFormat> _video = new List<VideoFormat>();
        public List<VideoFormat> Video
        {
            get
            {
                return _video;
            }
            set
            {
                Set(ref _video, value);
            }
        }

        private List<AudioFormat> _audio = new List<AudioFormat>();
        public List<AudioFormat> Audio { get => _audio; set => Set(ref _audio, value); }
        
        private List<MergedFormat> _merged = new List<MergedFormat>();
        public List<MergedFormat> Merged { get => _merged; set => Set(ref _merged, value); }

        private VideoFormat _selectedVideoFormat;
        public VideoFormat SelectedVideoFormat
        {
            get => _selectedVideoFormat;
            set
            {
                if (!Set(ref _selectedVideoFormat, value))
                    return;

                Media.Video = value;
            }
        }

        private AudioFormat _selectedAudioFormat;
        public AudioFormat SelectedAudioFormat 
        { 
            get => _selectedAudioFormat;
            set
            {
                if (!Set(ref _selectedAudioFormat, value))
                    return;

                Media.Audio = value;
            }
        }

        public RelayCommand ConvertCommand { get; set; }
        

        public MediaViewModel(INavigationService navigationService, Model.Media media)
        {
            _navigationService = navigationService;
            Media = media;
            ConvertCommand = new RelayCommand(OpenConvert, CanOpenConvert);
        }

        private void OpenConvert()
        {
            _navigationService.OpenWindow(new ConvertViewModel(_navigationService, Media));
        }

        private bool CanOpenConvert()
        {
            return IsDownloadable();
        }

        public bool IsDownloadable()
        {
            if (_selectedAudioFormat != null && _selectedVideoFormat != null && !String.IsNullOrEmpty(_url) && !String.IsNullOrEmpty(Name))
                return true;

            return false;
        }

        public bool IsDeletable()
        {
            if (String.IsNullOrEmpty(_url)) return true;
            return false;
        }

        public bool IsFilled()
        {
            if (!String.IsNullOrEmpty(_url) && !String.IsNullOrEmpty(Name))
                return true;
            return false;
        }

        public void Clear()
        {
            Video.Clear();
            Audio.Clear();
            SelectedAudioFormat = null;
            SelectedVideoFormat = null;
            Media.Clear();
        }
    }
}
