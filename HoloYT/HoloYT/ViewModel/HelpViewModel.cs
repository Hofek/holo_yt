﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using HoloYT.Messages;
using HoloYT.Model;
using HoloYT.Model.Interfaces;
using HoloYT.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HoloYT.ViewModel
{
    public class HelpViewModel : ViewModelBase
    {
        private string dllVersion;
        public string DllVersion
        {
            get { return dllVersion; }
            set
            {
                Set(ref dllVersion, value);
            }
        }

        private string programVersion;
        public string ProgramVersion
        {
            get { return programVersion; }
            set
            {
                Set(ref programVersion, value);
            }
        }

        public RelayCommand SwitchViewCommand_Main { get; set; }
        public IHelpDataService helpDataService { get; private set; }

        public HelpViewModel(IHelpDataService helpDataService)
        {
            SwitchViewCommand_Main = new RelayCommand(SwitchViewToMain);          
            this.helpDataService = helpDataService;
            LoadData();
        }

        private void LoadData()
        {
            DllVersion = helpDataService.GetDllVersion();
            ProgramVersion = helpDataService.GetProgramVersion();
        }

        private void SwitchViewToMain()
        {
            Messenger.Default.Send<SwitchViewMessage>(new SwitchViewMessage() { View = "Main" });
        }
    }
}
