﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using HoloYT.Model;
using HoloYT.Model.Formats;
using HoloYT.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace HoloYT.ViewModel
{
    public class ConvertViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private Model.Media _media;
        private int _width;
        private int _height;
        private List<string> _containers = Enum.GetNames(typeof(Enums.Container)).ToList();
        private string _selectedContainer;
        private List<string> _videoCodecs = Enum.GetNames(typeof(Enums.VideoCodec)).ToList();
        private string _selectedVideoCodec;
        private List<string> _audioCodecs = Enum.GetNames(typeof(Enums.AudioCodec)).ToList();
        private string _selectedAudioCodec;
        private List<string> _sampleRates = Enum.GetNames(typeof(Enums.SampleRate)).ToList();
        private string _selectedSampleRate;
        private bool _convert;
        private int _channelCount = 2;

        public ConvertViewModel(INavigationService navigationService, Media media)
        {
            _media = media;
            _navigationService = navigationService;

            SaveCommand = new RelayCommand(Save);
            CancelCommand = new RelayCommand(Cancel);
        }        

        private void Save()
        {
            _media.TargetFormat = new TargetFormat()
            {
                AudioCodec = _selectedAudioCodec,
                VideoCodec = _selectedVideoCodec,
                ChannelCount = _channelCount,
                Container = _selectedContainer,
                Resolution = String.Concat(_width, ' ', _height),
                SampleRate = _selectedSampleRate
            };
        }
       
        private void Cancel()
        {
            _navigationService.CloseWindow(this);
        }

        public ICollectionView Containers { get; set; }
        public string SelectedContainer { get => _selectedContainer; set => Set(ref _selectedContainer, value); }
        public ICollectionView VideoCodecs { get; set; }
        public string SelectedVideoCodec { get => _selectedVideoCodec; set => Set(ref _selectedVideoCodec, value); }
        public ICollectionView AudioCodecs { get; set; }
        public string SelectedAudioCodec { get => _selectedAudioCodec; set => Set(ref _selectedAudioCodec, value); }
        public int ChannelCount { get => _channelCount; set => Set(ref _channelCount, value); }
        public ICollectionView SampleRates { get; set; }
        public string SelectedSampleRate { get => _selectedSampleRate; set => Set(ref _selectedSampleRate, value); }
        public int Width { get => _width; set => Set(ref _width, value); }
        public int Height { get => _height; set => Set(ref _height, value); }
        public bool Convert
        {
            get => _convert;
            set
            {
                if (!Set(ref _convert, value))
                    return;

                if (!value)
                    return;

                if (_selectedVideoCodec != null)
                    return;

                Width = Int32.Parse(_media.Video?.Resolution.Split('x')[0]);
                Height = Int32.Parse(_media.Video?.Resolution.Split('x')[1]);
                SelectedVideoCodec = _videoCodecs[0];
                SelectedAudioCodec = _audioCodecs[0];
                SelectedContainer = _containers[0];
                SelectedSampleRate = _sampleRates[0];                
            }
        }
        public RelayCommand SaveCommand { get; set; }
        public RelayCommand CancelCommand { get; set; }
    }
}
