﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using HoloYT.Deserializer;
using HoloYT.Messages;
using HoloYT.Model;
using HoloYT.Model.Interfaces;
using System.IO;
using System.Net;

namespace HoloYT.ViewModel
{
    public class UpdateViewModel : ViewModelBase
    {
        private UpdateInfo _updateInfo;

        private string changelog;

        public string Changelog
        {
            get { return changelog; }
            set
            {
                Set(ref changelog, value);
            }
        }

        private string version;

        public string Version
        {
            get { return version; }
            set
            {
                if (value != null)
                    Set(ref version, value);
            }
        }

        private bool isUpdateAvaiable;

        public bool IsUpdateAvaiable
        {
            get { return isUpdateAvaiable; }
            set
            {
                Set(ref isUpdateAvaiable, value);
            }
        }

        private bool isUpdating;

        public bool IsUpdating
        {
            get { return isUpdating; }
            set
            {
                Set(ref isUpdating, value);
            }
        }       

        public RelayCommand UpdateCommand { get; set; }
        public RelayCommand SwitchToMainCommand { get; set; }

        private IUpdateDataService UpdateDataService;
        private IHelpDataService HelpDataService;

        public UpdateViewModel(IUpdateDataService updateDataService, IHelpDataService helpDataService)
        {
            UpdateCommand = new RelayCommand(Update, CanUpdate);
            SwitchToMainCommand = new RelayCommand(SwitchToMain, CanSwitchToMain);

            UpdateDataService = updateDataService;
            HelpDataService = helpDataService;
            CheckForUpdates();
            SwitchToMain();
        }

        private bool CanSwitchToMain()
        {
            if (IsUpdating == false) return true;
            return false;
        }

        private void SwitchToMain()
        {
            Messenger.Default.Send<SwitchViewMessage>(new SwitchViewMessage() { View = "Main" });
        }

        private bool CanUpdate()
        {
            if (IsUpdating == false) return true;
            return false;
        }

        private void CheckForUpdates()
        {
#if !DEBUG
            SwitchToMain();
#else         
            string html = string.Empty;
            string url = @"https://localhost:44398/api/updateyt";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                html = reader.ReadToEnd();
            }

            _updateInfo = JsonDeserializer.UpdateInfo(html);

            if (HelpDataService.GetProgramVersion() != _updateInfo.ProgramVersion)
                Update();
            
#endif
        }

        private void Update()
        {
            IsUpdateAvaiable = true;
        }
    }
}
