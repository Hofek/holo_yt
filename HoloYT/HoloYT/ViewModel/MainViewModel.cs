﻿using GalaSoft.MvvmLight;
using HoloYT.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using HoloYT.Messages;
using System.IO;
using System.Windows.Input;
using Ookii.Dialogs.Wpf;
using System.Threading.Tasks;
using HoloYT.Model.Interfaces;
using System.Windows;

namespace HoloYT.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.mvvmlight.net
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        #region Properties              

        private string _consoleContent;
        public string ConsoleContent
        {
            get => _consoleContent;
            set => Set(ref _consoleContent, value);
        }

        private bool debugConsole;
        public bool DebugConsole
        {
            get => debugConsole;
            set => Set(ref debugConsole, value);
        }


        private bool advancedMode;
        public bool AdvancedMode
        {
            get => advancedMode;
            set => Set(ref advancedMode, value);
        }

        private bool _blockUI;
        public bool BlockUI
        {
            get => _blockUI;
            set
            {
                if (!Set(ref _blockUI, value))
                    return;

                Application.Current.Dispatcher.BeginInvoke((Action)(() => { CommandManager.InvalidateRequerySuggested(); }));
            }
        }

        private ObservableCollection<MediaViewModel> _medias;
        public ObservableCollection<MediaViewModel> Medias { get { return _medias; } }
     
        private MediaViewModel _selectedItem;
        private readonly INavigationService _navigationService;

        public MediaViewModel SelectedItem
        {
            get => _selectedItem;

            set
            {
                if (_selectedItem != null)                
                    _selectedItem.PropertyChanged -= OnLinkChanged;                                                    

                Set(ref _selectedItem, value);
                if (_selectedItem != null)
                    _selectedItem.PropertyChanged += OnLinkChanged;
            }
        }

        #endregion

        #region Events
        private void OnLinkChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Url")
            {
                if (_medias.Count > 0)
                {
                    if (!String.IsNullOrWhiteSpace(_medias[_medias.Count - 1].Url)) Medias.Add(new MediaViewModel(_navigationService, new Media()));                  
                }
            }
        }
        #endregion

        public RelayCommand GetInfoCommand { get; set; }
        public RelayCommand DownloadLocalCommand { get; set; }
        public RelayCommand DownloadAdvancedCommand { get; set; }
        public RelayCommand RestartUICommand { get; set; }
        
        private IDataService _dataService { get; }

        private void Initialize()
        {
            ConsoleContent = "[INFO] Start programu" + Environment.NewLine;

            AdvancedMode = _dataService.IsInAdvancedMode;
            BlockUI = _dataService.IsUIBlocked;
            DebugConsole = _dataService.IsConsoleEnabled;        
            
            GetInfoCommand = new RelayCommand((Action)(()=> { _ = Download(); }), CanDownload);
            DownloadLocalCommand = new RelayCommand(DownloadLocal, CanDownloadAdvancedOrLocal);
            DownloadAdvancedCommand = new RelayCommand((Action)(()=> _ = DownloadAll()), CanDownloadAdvancedOrLocal);
            RestartUICommand = new RelayCommand(RestartUI, CanRestartUI);
            

            Messenger.Default.Register<AdvancedModeMessage>(this, SwitchAdvancedMode);
            Messenger.Default.Register<DebugConsoleMessage>(this, (message) => { DebugConsole = !debugConsole; });
            
            ConsoleContent = "[INFO] Inicjalizacja udana." + Environment.NewLine;
        }

        

        private bool CanRestartUI()
        {
            List<MediaViewModel> _links = _medias.ToList();
            if (BlockUI == false || (_links.FindAll(l => (l.State != Enums.MediaState.complete && l.State != Enums.MediaState.error)).Count > 0))
                return false;

            return true;
        }

        private void RestartUI()
        {
            List<MediaViewModel> medias = _medias.ToList();
            int index;
            while((index = medias.FindIndex(m => m.State == Enums.MediaState.complete))>=0)
            {
                Medias.RemoveAt(index);
                medias.RemoveAt(index);
            }
            if (Medias.Count == 0) Medias.Add(new MediaViewModel(_navigationService, new Media()));
            BlockUI = false;
        }        

        private bool CanDownloadAdvancedOrLocal()
        {
            List<MediaViewModel> _links = _medias.ToList();
            if (_links.FindAll(l => l.IsDownloadable() == false).Count > 0)                         
                return false;
                     
            return true;
        }              
               
        public MainViewModel(IDataService dataService, INavigationService navigationService)
        {
            _dataService = dataService;
            _navigationService = navigationService;
            _medias = new ObservableCollection<MediaViewModel>() { new MediaViewModel(_navigationService, new Media()) };
            Initialize();   
        }        
        
        private async Task GetMetaAll(IList<MediaViewModel> medias)
        {           
            var metaTasks = new List<Task>();
            for (int a = 0; a < medias.Count; ++a)            
                metaTasks.Add(_dataService.GetMeta(_medias[a]));
            

            await Task.WhenAll(metaTasks.ToArray());
            if (!await _dataService.VerifyOperation(medias))
                _ = GetMetaAll(medias.Where(l => l.State == Enums.MediaState.update).ToList());
        }

        private async Task DownloadAll(IList<MediaViewModel> medias = null, string path = null)
        {           
            if (medias == null)
            {
                await GetMetaAll(_medias);
                medias = _medias;
            }
                

            var downloadTasks = new List<Task>();
            for (int a = 0; a < medias.Count; ++a)            
                downloadTasks.Add(_dataService.Download(medias[a], path ?? null));
            

            await Task.WhenAll(downloadTasks.ToArray());
            if (!await _dataService.VerifyOperation(medias))
                _ = DownloadAll(medias.Where(l => l.State == Enums.MediaState.update).ToList());
        }

        private void DownloadLocal()
        {
            filterLinks();
            VistaFolderBrowserDialog folderBrowserDialog = new VistaFolderBrowserDialog();

            if (folderBrowserDialog.ShowDialog() ?? false)
                _ = DownloadAll(_medias.ToList().FindAll(l => l.State != Enums.MediaState.error), folderBrowserDialog.SelectedPath);
        }

        private async Task Download()
        {
            BlockUI = true;
            filterLinks();

            if (advancedMode == false)            
                await DownloadAll().ConfigureAwait(false);            
            else            
                await GetMetaAll(_medias).ConfigureAwait(false);
            
            BlockUI = false;
        }

        private void filterLinks()
        {
            List<MediaViewModel> temp = _medias.ToList();
            int index = -1;
            while ((index = temp.FindIndex(p => p.IsDeletable())) >= 0 && Medias.Count > 1)
            {
                Medias.RemoveAt(index);
                temp.RemoveAt(index);
            }
        }

        private bool CanDownload()
        {
            List<MediaViewModel> _links = _medias.ToList();
            if (_links.FindIndex(l => !l.IsFilled() && !l.IsDeletable()) >= 0)
                return false;
            if (_links.FindIndex(l => l.IsFilled()) >= 0)
                return true;

            return false;
        }

        private void SwitchAdvancedMode(AdvancedModeMessage obj)
        {
            AdvancedMode = !AdvancedMode;
        }
    }
}