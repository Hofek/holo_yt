﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using HoloYT.Messages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoloYT.ViewModel
{
    public class MasterViewModel : ViewModelBase
    {
        private object currentViewModel;
        public object CurrentViewModel
        {
            get
            {
                return currentViewModel;
            }
            set
            {               
                Set(ref currentViewModel, value);
            }
        }

        private bool advancedMode;
        public bool AdvancedMode
        {
            get
            {
                return advancedMode;
            }

            set
            {          
                Set(ref advancedMode, value);
                Messenger.Default.Send<AdvancedModeMessage>(new AdvancedModeMessage());
            }
        }

        private bool debugConsole;
        public bool DebugConsole
        {
            get
            {
                return debugConsole;
            }

            set
            {
                debugConsole = value;
                RaisePropertyChanged("DebugConsole");
                Messenger.Default.Send<DebugConsoleMessage>(new DebugConsoleMessage());
            }
        }

        public RelayCommand SwitchViewCommand_Options { get; set; }
        public RelayCommand SwitchViewCommand_Help { get; set; }

        public MasterViewModel()
        {         
            Messenger.Default.Register<SwitchViewMessage>(this, OnSwitchViewMessageReceived);
           
            SwitchViewCommand_Options = new RelayCommand(SwitchToOptions, CanSwitchToOptions);
            SwitchViewCommand_Help = new RelayCommand(SwitchToHelp, CanSwitchToHelp);
           
            CurrentViewModel = SimpleIoc.Default.GetInstance<MainViewModel>();

            CleanTemp();         
        }

        private bool CanSwitchToHelp()
        {
            if (CurrentViewModel != SimpleIoc.Default.GetInstance<HelpViewModel>()) return true;
            return false;
        }

        private void SwitchToHelp()
        {
            CurrentViewModel = SimpleIoc.Default.GetInstance<HelpViewModel>();
        }

        private void CleanTemp()
        {
            if (Directory.Exists(Environment.ExpandEnvironmentVariables(Properties.Settings.Default.TempFolder)))
            {
                string[] tempDirectory = Directory.GetFiles(Environment.ExpandEnvironmentVariables(Properties.Settings.Default.TempFolder));
                foreach (string fileName in tempDirectory)
                    File.Delete(fileName);
            }
        }
        

        private void OnSwitchViewMessageReceived(SwitchViewMessage obj)
        {
            switch(obj.View)
            {
                case "Main":
                    {
                        CurrentViewModel = SimpleIoc.Default.GetInstance<MainViewModel>();
                        break;
                    }
            }
        }

        private bool CanSwitchToOptions()
        {
            if (CurrentViewModel != SimpleIoc.Default.GetInstance<OptionsViewModel>()) return true;
            return false;
        }

        private void SwitchToOptions()
        {
            CurrentViewModel = SimpleIoc.Default.GetInstance<OptionsViewModel>();
        }
    }
}
