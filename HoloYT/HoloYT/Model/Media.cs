﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using static HoloYT.Model.Enums;
using HoloYT.Model.Formats;

namespace HoloYT.Model
{
    public class Media : INotifyPropertyChanged
    {
        public TargetFormat TargetFormat { get; set; }
        public VideoFormat Video { get; set; }       
        public AudioFormat Audio;
        
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (_name == value)
                    return;

                _name = value;
                OnPropertyRaised();
            }
        }
        
        public string Extension { get; set; }                                

        private MediaType _type;
        public MediaType Type
        {
            get
            {
                return _type;
            }

            set
            {
                if (_type == value)
                    return;

                _type = value;
                OnPropertyRaised();
            }
        }        

        public Media()
        {                        
            Video = new VideoFormat();
            Audio = new AudioFormat();            
        }        

        public void Clear()
        {            
            Video = null;
            Audio = null;
        }
        private bool _convert;
        public bool Convert 
        { 
            get
            {
                return _convert;
            }
            set
            {
                if (_convert == value)
                    return;
                _convert = value;
                OnPropertyRaised();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyRaised([CallerMemberName]string propertyname = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyname));
        }


    }
}
