﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoloYT.Model
{
    public static class Enums
    {
        public enum MediaType
        {
            unknown,
            youtube,
            facebook,
            instagram,
            twitter
        };

        public enum Container
        {
            mp4,
            mov,
            mkv,
            mxf
        }

        public enum VideoCodec
        {
            copy,
            libx264,
            ProRes,
            SDQuicktimeIMX50
        }

        public enum SampleRate
        {
            ar48000,
            ar44100
        }

        public enum AudioCodec
        {
            copy,
            aac,
            pcm_s16le,
            pcm_s32le
        }

        public enum MediaState
        {
            none,
            complete,
            download,
            convert,
            getInfo,
            error,
            update
        }
    }
}
