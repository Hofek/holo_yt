﻿using HoloYT.Model.Interfaces;
using HoloYT.ViewModel;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace HoloYT.Model
{
    public abstract class MediaProviderBase : IMediaProvider
    {
        protected static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        protected virtual string[] FindPreferred(MediaViewModel mediaVm)
        {
            string[] codes = new string[2];
            codes[0] = null;
            codes[1] = null;

            int index = -1;
            try
            {
                if (mediaVm.AudioOnly == false)
                {
                    if ((index = mediaVm.Merged.FindLastIndex(p => p.Resolution.Contains(Config.Instance.TargetFormat.Resolution))) > -1)
                    {
                        codes[0] = mediaVm.Merged[index].Code;
                        codes[1] = null;
                        return codes;
                    }
                    else if ((index = mediaVm.Video.FindLastIndex(p => p.Resolution.Contains("x720"))) > -1)
                    {
                        codes[0] = mediaVm.Video[index].Code;
                    }
                    else
                    {
                        int[,] resolutions = new int[mediaVm.Video.Count, 2];
                        string[] temp = new string[2];
                        int maxX = -1;

                        for (int a = 0; a < mediaVm.Video.Count; ++a)
                        {
                            temp = mediaVm.Video[a].Resolution.Split('x');
                            if (maxX < Int32.Parse(temp[0]))
                            {
                                maxX = Int32.Parse(temp[0]);
                                codes[0] = mediaVm.Video[a].Code;
                            }
                        }

                        for (int a = 0; a < mediaVm.Merged.Count; ++a)
                        {
                            temp = mediaVm.Merged[a].Resolution.Split('x');
                            if (maxX <= Int32.Parse(temp[0]))
                            {
                                maxX = Int32.Parse(temp[0]);
                                codes[0] = mediaVm.Merged[a].Code;
                                codes[1] = null;
                                return codes;
                            }
                        }
                    }
                }

                if ((index = mediaVm.Audio.FindLastIndex(p => p.Container.Contains("m4a"))) > -1)
                {
                    codes[1] = mediaVm.Audio[index].Code;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, $"Error when finding target version. ({mediaVm.Name})");
            }

            return codes;
        }
        
        protected string StripSpecialChars(string x)
        {
            for (int a = 0; a < x.Length; ++a)
                if (!((x[a] > 47 && x[a] < 58) || (x[a] > 64 && x[a] < 91) || (x[a] > 96 && x[a] < 123) || x[a] == 32 || x[a] == 95))
                    x = x.Remove(a, 1);
            return x;
        }
        public abstract Task<bool> DownloadMedia(MediaViewModel mediaVm, bool auto = true);       

        public abstract Task<bool> GetMediaInfo(MediaViewModel mediaVm);

        
    }
}
