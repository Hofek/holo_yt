﻿using HoloYT.Extensions;
using HoloYT.Model.Formats;
using HoloYT.Model.Interfaces;
using System;
using System.IO;
using System.Xml.Serialization;

namespace HoloYT.Model
{
    [Serializable]
    public class Config
    {
        [XmlIgnore]
        private static Config Singleton;

        public static Config Instance { get { return Singleton; } }
               
        static Config()
        {
            Singleton = Load();
        }
        public Config()
        {
            Proxy = String.Empty;
            VideoPath = String.Empty;
            AudioPath = String.Empty;
            TargetFormat = new TargetFormat();
        }

        //public Config(Config config)
        //{
        //    Proxy = config.Proxy;
        //    VideoPath = config.VideoPath;
        //    AudioPath = config.AudioPath;
        //    LibraryVersion = config.LibraryVersion;            
        //}

        private static Config Load()
        {
            if (!Directory.Exists(Environment.ExpandEnvironmentVariables(Properties.Settings.Default.SettingsPath)))
                Directory.CreateDirectory(Environment.ExpandEnvironmentVariables(Properties.Settings.Default.SettingsPath));

            if (!Directory.Exists(Environment.ExpandEnvironmentVariables(Properties.Settings.Default.TempFolder)))
                Directory.CreateDirectory(Environment.ExpandEnvironmentVariables(Properties.Settings.Default.TempFolder));

            if (File.Exists(Environment.ExpandEnvironmentVariables(Properties.Settings.Default.ConfigFile)))
            {
                XmlSerializer xmlserializer = new XmlSerializer(typeof(Config));
                using (StreamReader sr = new StreamReader(Environment.ExpandEnvironmentVariables(Properties.Settings.Default.ConfigFile)))
                {
                    return (Config)xmlserializer.Deserialize(sr);
                }
            }
            else
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Config));
                using (StreamWriter sw = new StreamWriter(Environment.ExpandEnvironmentVariables(Properties.Settings.Default.ConfigFile)))
                {
                    Config.Instance.Proxy = "proxy.tvp.pl:80";
                    Config.Instance.VideoPath = String.Empty;
                    Config.Instance.AudioPath = String.Empty;
                    serializer.Serialize(sw, Config.Instance);
                }
            }
            return Config.Instance;
        }
        public static void Save()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Config));
            using (StreamWriter sw = new StreamWriter(Environment.ExpandEnvironmentVariables(Properties.Settings.Default.ConfigFile)))
            {
                serializer.Serialize(sw, Instance);
            }
        }

        [XmlElement("Proxy")]
        public string Proxy { get; set; }

        [XmlElement("VideoPath")]
        public string VideoPath { get; set; }
        [XmlElement("AudioPath")]
        public string AudioPath { get; set; }

        [XmlIgnore]
        public string LibraryVersion { get; set; }  
        public TargetFormat TargetFormat { get; set; }
    }
}
