﻿using HoloYT.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HoloYT.Model.Interfaces
{
    public interface IDataService
    {
        bool IsInAdvancedMode { get; }
        bool IsUIBlocked { get; }
        bool IsConsoleEnabled { get; }        
        
        Task<bool> UpdateLibrary();
        Task<bool> GetMeta(MediaViewModel media);
        Task<bool> Download(MediaViewModel media, string path=null);
        Task<bool> VerifyOperation(IList<MediaViewModel> medias);
    }
}
