﻿using HoloYT.ViewModel;
using System.Threading.Tasks;

namespace HoloYT.Model.Interfaces
{
    public interface IMediaProvider
    {        
        Task<bool> DownloadMedia(MediaViewModel mediaVm, bool auto=true);
        Task<bool> GetMediaInfo(MediaViewModel mediaVm);
    }
}
