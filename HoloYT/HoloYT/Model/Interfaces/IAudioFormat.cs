﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoloYT.Model.Interfaces
{
    public interface IAudioFormat
    {                
        string Sampling { get; set; }
    }
}
