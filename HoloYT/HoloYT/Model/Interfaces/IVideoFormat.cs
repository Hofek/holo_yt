﻿using System;
namespace HoloYT.Model.Interfaces
{
    public interface IVideoFormat
    {       
        string Resolution { get; set; }
        string Fps { get; set; }
    }
}
