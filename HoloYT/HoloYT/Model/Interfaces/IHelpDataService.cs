﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoloYT.Model.Interfaces
{
    public interface IHelpDataService
    {
        string GetProgramVersion();
        string GetDllVersion();
    }
}
