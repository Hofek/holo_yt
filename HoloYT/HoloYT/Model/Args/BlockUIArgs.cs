﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoloYT.Model.Args
{
    public class BlockUIArgs
    {
        public bool IsBlocked { get; }
        public BlockUIArgs(bool isBlocked)
        {
            IsBlocked = isBlocked;
        }
    }
}
