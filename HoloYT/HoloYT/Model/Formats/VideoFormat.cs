﻿using HoloYT.Model.Interfaces;
using System.ComponentModel;
using System.Runtime.CompilerServices;


namespace HoloYT.Model.Formats
{
    public class VideoFormat : FormatBase, INotifyPropertyChanged, IVideoFormat
    {
        public string Fps { get; set; }
        private string _resolution { get; set; }        
        public string Resolution
        {
            get
            {
                return _resolution;
            }
            set
            {
                _resolution = value;
                OnPropertyRaised();
            }
        }       

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyRaised([CallerMemberName]string propertyname = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyname));
        }
    }
}
