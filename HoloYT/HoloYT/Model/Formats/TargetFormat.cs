﻿namespace HoloYT.Model.Formats
{
    public class TargetFormat
    {
        public string Resolution { get; set; }
        public string Container { get; set; }
        public string VideoCodec { get; set; }
        public string AudioCodec { get; set; }
        public int ChannelCount { get; set; }
        public string SampleRate { get; set; }

        public TargetFormat()
        {

        }
        public TargetFormat(string resolution, string container, string videoCodec, string audioCodec, int channelCount, string sampleRate)
        {
            Resolution = resolution;
            Container = container;
            VideoCodec = videoCodec;
            AudioCodec = audioCodec;
            ChannelCount = channelCount;
            SampleRate = sampleRate;
        }
    }
}
