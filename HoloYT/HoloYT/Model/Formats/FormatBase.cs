﻿namespace HoloYT.Model.Formats
{
    public abstract class FormatBase
    {
        public string Code { get; set; }
        public string Codec { get; set; }
        public string Container { get; set; }
        public string Bitrate { get; set; }        
    }
}
