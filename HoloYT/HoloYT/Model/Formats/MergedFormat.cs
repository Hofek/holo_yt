﻿using HoloYT.Model.Interfaces;

namespace HoloYT.Model.Formats
{
    public class MergedFormat : FormatBase, IAudioFormat, IVideoFormat
    {                       
        public string Sampling { get; set; }
        public string Resolution { get; set; }
        public string Fps { get; set; }
    }
}
