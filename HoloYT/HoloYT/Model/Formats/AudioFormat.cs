﻿using HoloYT.Model.Interfaces;

namespace HoloYT.Model.Formats
{
    public class AudioFormat : FormatBase, IAudioFormat
    {       
        public string Sampling { get; set; }        
    }
}
