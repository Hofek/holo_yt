﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoloYT.Model
{
    public class UpdateInfo
    {
        public string ProgramVersion { get; set; }
        public string ApiVersion { get; set; }
    }
}
