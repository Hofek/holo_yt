﻿using HoloYT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoloYT.Deserializer
{
    public class JsonDeserializer
    {
        public static  UpdateInfo UpdateInfo(string json)
        {
            UpdateInfo _updateInfo = new UpdateInfo();
            _updateInfo.ProgramVersion = json.Replace("{", "").Replace("}", "").Split(',')[0].Split(':')[1].Replace("\"", "");
            _updateInfo.ApiVersion = json.Replace("{", "").Replace("}", "").Split(',')[1].Split(':')[1].Replace("\"", "");

            return _updateInfo;
        }
    }
}
