﻿using HoloYT.Model;
using HoloYT.Model.Formats;
using HoloYT.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace HoloYT.MediaProviders
{
    public class FacebookProvider : MediaProviderBase
    {       
        public override Task<bool> DownloadMedia(MediaViewModel mediaVm, bool auto = true)
        {
            mediaVm.Name = StripSpecialChars(mediaVm.Name);
            string line;
            var taskState = new TaskCompletionSource<bool>();
            string codes = mediaVm.AudioOnly ? mediaVm.Media.Audio.Code : String.Concat(mediaVm.Media.Video, '+', mediaVm.Media.Audio.Code);
            try
            {
                if (auto)
                {
                    string[] temp = FindPreferred(mediaVm);
                    if (mediaVm.AudioOnly)
                        codes = temp[1];
                    else
                        codes = String.Concat(temp[0], temp[1] == null ? "" : String.Concat("+", temp[1]));
                }


                if (codes != null && mediaVm.State != Enums.MediaState.error)
                {
                    Process proc = new Process
                    {
                        StartInfo = new ProcessStartInfo
                        {
                            FileName = "api/youtube-dl.exe",
                            Arguments = "--ffmpeg-location api/ffmpeg.exe -f " + codes + (String.IsNullOrEmpty(Config.Instance.Proxy) ? "" : (" --proxy " + Config.Instance.Proxy)) + " -o \"" + Environment.ExpandEnvironmentVariables(Properties.Settings.Default.TempFolder) + "\\" + mediaVm.Name + ".%(ext)s\" " + mediaVm.Url,
                            UseShellExecute = false,
                            RedirectStandardOutput = true,
                            RedirectStandardError = true,
                            CreateNoWindow = true
                        }
                    };

                    proc.EnableRaisingEvents = true;
                    proc.OutputDataReceived += new DataReceivedEventHandler((sender, e) =>
                    {
                        if (!String.IsNullOrEmpty(e.Data))
                        {
                            line = e.Data;
                            if (line.Contains("[download]") && line.Contains("%"))
                            {
                                line = System.Text.RegularExpressions.Regex.Replace(line, @"\s+", " ");
                                line = line.Remove(line.IndexOf('%'));
                                if (line.Contains(".")) 
                                    line = line.Remove(line.IndexOf('.'));
                                line = line.Remove(0, 11);

                                Application.Current.Dispatcher.BeginInvoke((Action)(() =>
                                {
                                    if (Int32.TryParse(line, out var percentage))
                                        mediaVm.Progress = percentage;
                                }));
                            }
                            else if (line.Contains("[download]") && line.Contains("Destination"))
                            {
                                mediaVm.Media.Extension = line.Substring(line.LastIndexOf('.') + 1);
                            }
                            else if (line.Contains("[ffmpeg]") && line.Contains("Merging"))
                            {
                                mediaVm.Media.Extension = line.Substring(line.LastIndexOf('.') + 1);
                                mediaVm.Media.Extension = mediaVm.Media.Extension.Replace("\"", "");
                            }
                        }
                    });
                    proc.Exited += (s, e) => { taskState.SetResult(true); };
                    proc.ErrorDataReceived += new DataReceivedEventHandler((sender, e) =>
                    {
                        if (String.IsNullOrEmpty(e.Data))
                            return;
                        line = e.Data;
                        if (line.ToLower().Contains("error"))
                        {
                            //UpdateState("[Błąd] " + Thread.CurrentThread.Name + ": " + line);
                            mediaVm.State = Enums.MediaState.error;
                            proc.Close();
                            proc.Dispose();
                            taskState.SetResult(false);
                        }
                    });

                    proc.Start();
                    proc.BeginOutputReadLine();
                    proc.BeginErrorReadLine();

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error when downloading media from Youtube. ({0})", mediaVm.Name);
                taskState.SetResult(false);
            }

            return taskState.Task;
        }      

        public override Task<bool> GetMediaInfo(MediaViewModel mediaVm)
        {
            mediaVm.Clear();
            mediaVm.Name = StripSpecialChars(mediaVm.Name);

            int index;
            if ((index = mediaVm.Url.IndexOf('&')) > -1)
                mediaVm.Url = mediaVm.Url.Substring(0, index);


            string line;
            var taskState = new TaskCompletionSource<bool>();

            List<string> firstSplit_Elements = new List<string>();
            List<string> secondSplit_Elements = new List<string>();

            Process proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "api/youtube-dl.exe",
                    Arguments = (String.IsNullOrEmpty(Config.Instance.Proxy) ? "" : (" --proxy " + Config.Instance.Proxy)) + " -F " + mediaVm.Url,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true
                }
            };
            proc.EnableRaisingEvents = true;
            proc.OutputDataReceived += (sender, e) =>
            {
                try
                {
                    if (String.IsNullOrEmpty(e.Data))
                        return;

                    line = e.Data;
                    if ((int)line[0] < 58 && (int)line[0] > 47)
                    {
                        firstSplit_Elements.Clear();
                        secondSplit_Elements.Clear();

                        firstSplit_Elements = line.Split(',').Select(p=>p.Trim()).ToList(); //easier to read codecs                        
                        secondSplit_Elements = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList(); //easier to read resolution and codes

                        if (line.Contains("audio only"))
                        {
                            var f = new AudioFormat()
                            {
                                Code = secondSplit_Elements[0],
                                Codec = (firstSplit_Elements[firstSplit_Elements.Count - 1].Contains(' ') ? 
                                firstSplit_Elements[firstSplit_Elements.Count - 1].Substring(0, firstSplit_Elements[firstSplit_Elements.Count - 1].IndexOf(' ')) : 
                                firstSplit_Elements[firstSplit_Elements.Count - 1]),
                                Sampling = secondSplit_Elements.Last().Replace("(","").Replace(")",""),
                                Container = secondSplit_Elements[1],
                                Bitrate = secondSplit_Elements[6]
                            };
                            mediaVm.Audio.Add(f);

                        }
                        else if (line.Contains("video only"))
                        {
                            var f = new VideoFormat()
                            {
                                Code = secondSplit_Elements[0],
                                Codec = (firstSplit_Elements[firstSplit_Elements.Count - 3].Contains('.') ? 
                                firstSplit_Elements[firstSplit_Elements.Count - 3].Substring(0, firstSplit_Elements[firstSplit_Elements.Count - 3].IndexOf('.')) : 
                                firstSplit_Elements[firstSplit_Elements.Count - 3]),
                                Bitrate = secondSplit_Elements[6],
                                Resolution = secondSplit_Elements[2],
                                Container = secondSplit_Elements[1],
                                Fps = firstSplit_Elements[firstSplit_Elements.Count - 2]
                            };
                            mediaVm.Video.Add(f);
                        }                        
                    }

                }
                catch (Exception ex)
                {
                    _logger.Error(ex, $"Error while reading MetaInfo for {mediaVm.Name}");
                }
            };
            proc.Exited += (s, e) => { taskState.SetResult(true); };
            proc.ErrorDataReceived += new DataReceivedEventHandler((sender, e) =>
            {
                if (!String.IsNullOrEmpty(e.Data))
                {
                    line = e.Data;
                    if (line.ToLower().Contains("error"))
                    {
                        //UpdateState("[Błąd] " + Thread.CurrentThread.Name + ": " + line);
                        mediaVm.State = Enums.MediaState.error;
                        proc.Close();
                        proc.Dispose();
                    }
                }
            });

            proc.Start();
            proc.BeginErrorReadLine();
            proc.BeginOutputReadLine();

            return taskState.Task;
        }
    }
}
