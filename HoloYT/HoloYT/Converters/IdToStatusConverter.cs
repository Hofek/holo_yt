﻿using HoloYT.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace HoloYT.Converters
{
    class IdToStatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
        object parameter, CultureInfo culture)
        {
            switch (value)
            {
                case Enums.MediaState.none:
                    {
                        return "Bezczynność";
                    }
                case Enums.MediaState.error:
                    {
                        return "Błąd";
                    }
                case Enums.MediaState.complete:
                    {
                        return "Gotowe";
                    }
                case Enums.MediaState.download:
                    {
                        return "Pobieranie...";
                    }
                case Enums.MediaState.convert:
                    {
                        return "Konwertowanie...";
                    }
                case Enums.MediaState.getInfo:
                    {
                        return "Zbieranie informacji...";
                    }
                case Enums.MediaState.update:
                    {
                        return "Aktualizacja...";
                    }
            }
            return "Nieznany status";
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
