﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows;

namespace HoloYT_Updater
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private async void Application_Startup(object sender, StartupEventArgs e)
        {
            if (e.Args.Length < 1)
                return;

            if (e.Args[0] != "--update")
               return;

            await Task.Delay(1000);

            Process[] proc = Process.GetProcessesByName("HoloYT");            
            foreach (Process p in proc)
            {
                p.Kill();
                p.WaitForExit();
            }                      

            if (!Directory.Exists(".\\temp"))
                Directory.CreateDirectory(".\\temp");           
            
            string zipPath = @".\\temp\\update.zip";          

            WebClient webClient = new WebClient();
            webClient.Headers.Add("Accept: text/html, application/xhtml+xml, */*");
            webClient.Headers.Add("User-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)");
            await webClient.DownloadFileTaskAsync(new Uri("https://localhost:44398/updateyt/package.zip"), zipPath);

            if (File.Exists("..\\HoloYT.exe"))
            {
                var files = Directory.GetFiles("..\\");
                var directories = Directory.GetDirectories("..\\");
                var parentDirectory = Directory.GetParent(".\\");

                foreach (string f in files)
                {
                    try
                    {
                        File.Delete(f);
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    
                }

                foreach (string d in directories)
                {
                    try
                    {
                        if (d != parentDirectory.FullName)
                            Directory.Delete(d, true);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }

                try
                {
                    using (ZipArchive zip = ZipFile.OpenRead(zipPath))
                    {
                        foreach(ZipArchiveEntry entry in zip.Entries)
                        {
                            if (entry.FullName[entry.FullName.Length-1] != '\\')
                                entry.ExtractToFile("..\\" + entry.FullName, true);
                        }
                    }
                    
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                var process = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = "..\\HoloYT.exe",
                        Arguments = "",
                    }
                };

                process.Start();
                Application.Current.Shutdown();
            }
            else
                MessageBox.Show("Nie znaleziono HoloYT.exe");
            
        }
    }
}
